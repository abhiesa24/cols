<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 29/09/2018
 * Time: 23:40
 */

class Register_model extends CI_Model
{
	public function insertData($data){
		$this->db->insert('users',$data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}

	public function insertSession($data){
		$this->db->insert('session', $data);
		return $this->db->affected_rows();
	}

	public function FindToken($data){
		$this->db->select('idUser');
		$this->db->from('session');
		$this->db->where(array('token' => $data,'exp' => '1'));
		$result = $this->db->get();
		return $result->result_array();
	}

	public function Confirm($data,$flag){
		$this->db->where('idUser', $data);
		$this->db->update('users', $flag);
		return $this->db->affected_rows();
	}

	public function ExpToken($data,$exp){
		$this->db->where('idUser', $data);
		$this->db->update('session', $exp);
		return $this->db->affected_rows();
	}

	public function getEmail($email){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('email' => $email));
		return $this->db->get()->result_array();
	}

	public function setToken($idUser,$token){
		$this->db->where('idUser', $idUser);
		$this->db->update('session', $token);
		return $this->db->affected_rows();
	}

	public function getDataUser($token){
		$this->db->select('*');
		$this->db->from('session');
		$this->db->join('users','users.idUser = session.idUser', 'left');
		$this->db->where('session.token', $token);

		return $this->db->get()->result_array();
	}

	public function setNewPassword($idUser,$password){
		$this->db->where('idUser', $idUser);
		$this->db->update('users', $password);
		return $this->db->affected_rows();
	}
}
