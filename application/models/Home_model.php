<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 02/10/2018
 * Time: 15:45
 */

class Home_model extends CI_Model
{
	public function getListener($token){

		$this->db->select(' followed.idUser');
		$this->db->from('followed');
		$this->db->join('users','users.idUser = followed.idUser', 'left');
		$this->db->join('session','session.idUser = users.idUser', 'left');
		$this->db->where('token',$token);
		return $this->db->get()->result_array();
	}
}
