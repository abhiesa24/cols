<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 01/10/2018
 * Time: 19:44
 */

class Login_model extends CI_Model{

	public function loginUser($email, $password){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('email' => $email, 'password' => $password, 'flag' => "1"));
		return $this->db->get()->result_array();
	}

	public function saveToken($id,$data){
		$this->db->where('idUser', $id);
		$this->db->update('session', $data);
		return $this->db->affected_rows();
	}

	public function getData($idUser){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('session','session.idUser = users.idUser','left');
		$this->db->where('users.idUser',$idUser);
		return $this->db->get()->result_array();
	}

}
