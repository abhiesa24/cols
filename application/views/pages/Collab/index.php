<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Botstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>open-iconic-master/font/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>css/colab.css">
	<link rel="stylesheet" href="<?= base_url('/assets/') ?>css/main.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	

    <title>Collab</title>
</head>
<body>

	<!-- navbar -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top">
		<div class="container-fluid">
  		<a class="navbar-brand" href="home.html">
    		<img src="<?= base_url('/assets/') ?>img/logo.png" width="30" height="30" alt="">
  		</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   		<span class="navbar-toggler-icon"></span>
  		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mx-auto">
				<li class="nav-item">
		        	<form class="navbar-form" role="search">
        				<div class="input-group">
            				<input type="text" class="form-control" placeholder="Search" name="q">
            				<div class="input-group-btn">
                			<button class="btn btn-default" type="submit"><i class="oi oi-magnifying-glass"></i>
                			</button>
            					</div>
        				</div>
        			</form>
		  		</li>
		  	</ul>

		  	<ul class="navbar-nav">
			    <li class="nav-item dropdown">
				    <a class="nav-link dropdown-toggle" id="notification" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
				        <i class="oi"><img src="<?= base_url('/assets/') ?>icon/si-glyph-bell.svg" class="img-fluid">
				          <span class="badge badge-danger">25</span>
				        </i>
				    </a>
				    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="notification">
				          <a class="dropdown-item" href="#">Action</a>
				          <a class="dropdown-item" href="#">Another action</a>
				          <a class="dropdown-item" href="#">Something else here</a>
			        </div>
				</li>

			    <li class="nav-item dropdown">
				    <a class="nav-link dropdown-toggle" id="message" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
				    	<i class="oi"><img src="<?= base_url('/assets/') ?>icon/si-glyph-mail.svg" class="img-fluid">
				          <span class="badge badge-danger">25</span>
				        </i>
				    </a>
				        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="message">
				          <a class="dropdown-item" href="#">Action</a>
				          <a class="dropdown-item" href="#">Another action</a>
				          <a class="dropdown-item" href="#">Something else here</a>
			        	</div>
				</li>

			  	<li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle oi oi-menu" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			        </a>
			        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="<?= base_url('Profile') ?>">Profile</a>
						<a class="dropdown-item" href="<?= base_url('Login/loogOut') ?>">Logout</a>
			        </div>
			    </li>

			</ul>
		</div>
		</div>
	</nav>

  <!-- !navbar -->



  <div class="container">
  	<div class="row">
	    <div class="col-lg-12" id="colabmain1">
	    	<ul class="nav nav-tabs">
			  <li class="nav-item" id="dashboardbtntabs">
			    <a class="nav-link active" data-toggle="tab" href="#dashboard"><span class="oi oi-menu"></span>Dashboard</a>
			  </li>
			  <li class="nav-item" id="discussions">
			    <a class="nav-link" data-toggle="tab" href="#menu1"><img src="<?= base_url('/assets/') ?>icon/si-glyph-bubble-message-text.svg" class="img-fluid">Discussions</a>
			  </li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane container active" id="dashboard">
			  	<div class="row">
	    		<div class="col-lg-2">
	    			<img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
	    		</div>
	    		<div class="col-lg-4">
	    			<div class="row">
	    				<div class="col-lg-12">
	    					<h1>Wherever You Are - Drum</h1>
	    				</div>
	    				<div class="col-lg-12">
	    					<h6>Alif</h6>
	    				</div>
						

	    			</div>
	    		</div>
	    		<div class="col-lg-1" id="buttonplay">
	    			<div class="tombolplay" data-toggle="buttons">
						<label class="btn btn-lg active">
						    <input type="radio" name="options" id="option1" autocomplete="off" checked>
						        <i><img onclick="pause()" src="<?= base_url('/assets/') ?>icon/si-glyph-pause.svg"/></i>
						</label>
						<label class="btn btn-lg">
						    <input type="radio" name="options" id="option2" autocomplete="off">
						        <i><img onclick="play()" src="<?= base_url('/assets/') ?>icon/si-glyph-triangle-right.svg"/></i>
						</label>          
					</div>
	    		</div>
	    		<div class="col-lg-5" id="soundWave">


					<div id="waveform1"></div>

	    		</div>	
	    	</div>
	    	<!--<div class="row" id="kelompokdetail">-->
	    		<!--<div class="col-lg-3">-->
	    			<!--<h9>Need help with...</h9>-->
	    		<!--</div>-->
	    		<!--<div class="col-lg-3 text-center">-->
	    			<!--<h9>As of...</h9>-->
	    		<!--</div>-->
	    		<!--<div class="col-lg-2 text-center">-->
	    			<!--<h9>Accepted</h9>-->
	    		<!--</div>-->
	    		<!--<div class="col-lg-2 text-center">-->
	    			<!--<h9>Ideas</h9>-->
	    		<!--</div>-->
	    		<!--<div class="col-lg-2 text-center">-->
	    			<!--<h9>Upload</h9>-->
	    		<!--</div>-->
	    	<!--</div>-->


				  <div style="margin-top:0px" id="playlist"></div>
				  <form id="volume" class="form-inline">
					  <div class="form-group">

						  <input type="range" min="0" max="100" value="100" class="master-gain form-control" id="master-gain">
					  </div>
				  </form>
				  <div id="tombolAnu" class="playlist-toolbar">
					  <div class="btn-group">
								<span class="btn-pause btn btn-warning">
									<i class="fa fa-pause"></i>
								</span>
						  <span class="btn-play btn btn-success">
									<i class="fa fa-play"></i>
								</span>
						  <span class="btn-stop btn btn-danger">
									<i class="fa fa-stop"></i>
								</span>
						  <span class="btn-rewind btn btn-success">
									<i class="fa fa-fast-backward"></i>
								</span>
						  <span class="btn-fast-forward btn btn-success">
									<i class="fa fa-fast-forward"></i>
								</span>
					  </div>
				  </div>


				  <div style="margin-bottom: 30px" class="row">
	    		<div class="col-lg-12">

	    			<div class="container">
					    <div class="card" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"" id="listtoolmusik">
					      <div class="row">
					        <div class="col-lg-3" id="namatoolmusik">
					            <h5>Upload Music</h5>
					        </div>
					        <div class="col-lg-3 text-center">
					            <h8 class="text-muted"></h8>
					            <h8></h8>
					        </div>
					        <div class="col-lg-2 text-center">
					            <h8></h8>
					        </div>
					        <div class="col-lg-2 text-center">
					            <h8></h8>
					        </div>
					        <div class="col-lg-2 text-center">
					            <button type="button" class="btn btn-success"><span class="oi oi-plus"></span></button>
					         </div>
					        </div>
					        <div class="row">
  								<div class="col-lg-12">
    								<div class="collapse" id="collapseExample">
									  <div class="card bg-transparent card card-body">

										  <div style="width:inherit; height:50px" class="track-drop"></div>
									    <!--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.-->
									  </div>
									</div>
  								</div>
							</div>
			   			</div>
					</div>

					<!--<div class="container">-->
					    <!--<div class="card" data-toggle="collapse" data-target="#collapseExamplecollapse" aria-expanded="false" aria-controls="collapseExample"" id="listtoolmusik">-->
					      <!--<div class="row">-->
					        <!--<div class="col-lg-3" id="namatoolmusik">-->
					            <!--<h5>Guitar</h5>-->
					        <!--</div>-->
					        <!--<div class="col-lg-3 text-center">-->
					            <!--<h8 class="text-muted">6 days</h8>-->
					            <!--<h8>ago</h8>-->
					        <!--</div>-->
					        <!--<div class="col-lg-2 text-center">-->
					            <!--<h8>-</h8>-->
					        <!--</div>-->
					        <!--<div class="col-lg-2 text-center">-->
					            <!--<h8>-</h8>-->
					        <!--</div>-->
					        <!--<div class="col-lg-2 text-center">-->
					            <!--<button type="button" class="btn btn-success"><span class="oi oi-plus"></span></button>-->
					         <!--</div>-->
					        <!--</div>-->
					        <!--<div class="row">-->
  								<!--<div class="col-lg-12">-->
    								<!--<div class="collapse" id="collapseExamplecollapse">-->
									  <!--<div class="card card-body">-->
									    <!--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.-->
									  <!--</div>-->
									<!--</div>-->
  								<!--</div>-->
							<!--</div>-->
			   			<!--</div>-->
					</div>

	    		</div>
	    	</div>
	    	<!--<div class="row" id="addtrack">-->
	    		<!--<div class="col-lg-3 offset-lg-9">-->
	    			<!--<h8 class="text-muted">Contribute other track</h8>-->
	    			<!--<button type="button" class="btn btn-success"><span class="oi oi-plus"></span></button>-->
	    		<!--</div>-->

	    	<!--</div>-->

			  </div>
			  <div class="tab-pane container fade" id="menu1">
			  	

			  </div>
			</div>




	    </div>

	</div>  	
  </div>

  <div class="container">
  	<div class="row">
	    <div class="col-lg-6" id="latesactivitycontainer">
	    	<div class="row">
	    		<div class="col-lg-12">
			    	<div class="row">
			    		<div class="col-lg-12" id="latesactivity">
			    			<h3>Latest Activity</h3>
			    		</div>
			    	</div>
			    	<div class="row">
			    		<div class="col-lg-12 card bg-transparent" id="latesactivitybody">

			    			<div class="container">
							    <div class="card bg-transparent" id="listactivity">
							      <div class="row">
							        <div class="col-lg-3" id="fotolatesactivity">
							            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
							        </div>
							        <div class="col-lg-9">
							        	<div class="row">
							        		<div class="col-lg-12">
							        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
							        		</div>
							        		<div class="col-lg-12">
							        			<h5 class="text-muted">1mo+24dy ago</h5>   
							        		</div>
							        	</div> 
							        </div>		        
							      </div>
					   			</div>
							</div>

							<div class="container">
							    <div class="card bg-transparent" id="listactivity">
							      <div class="row">
							        <div class="col-lg-3" id="fotolatesactivity">
							            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
							        </div>
							        <div class="col-lg-9">
							        	<div class="row">
							        		<div class="col-lg-12">
							        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
							        		</div>
							        		<div class="col-lg-12">
							        			<h5 class="text-muted">1mo+24dy ago</h5>   
							        		</div>
							        	</div> 
							        </div>		        
							      </div>
					   			</div>
							</div>

							<div class="container">
							    <div class="card bg-transparent" id="listactivity">
							      <div class="row">
							        <div class="col-lg-3" id="fotolatesactivity">
							            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
							        </div>
							        <div class="col-lg-9">
							        	<div class="row">
							        		<div class="col-lg-12">
							        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
							        		</div>
							        		<div class="col-lg-12">
							        			<h5 class="text-muted">1mo+24dy ago</h5>   
							        		</div>
							        	</div> 
							        </div>		        
							      </div>
					   			</div>
							</div>

							<div class="container">
							    <div class="card bg-transparent" id="listactivity">
							      <div class="row">
							        <div class="col-lg-3" id="fotolatesactivity">
							            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
							        </div>
							        <div class="col-lg-9">
							        	<div class="row">
							        		<div class="col-lg-12">
							        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
							        		</div>
							        		<div class="col-lg-12">
							        			<h5 class="text-muted">1mo+24dy ago</h5>   
							        		</div>
							        	</div> 
							        </div>		        
							      </div>
					   			</div>
							</div>

							<div class="container">
							    <div class="card bg-transparent" id="listactivity">
							      <div class="row">
							        <div class="col-lg-3" id="fotolatesactivity">
							            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
							        </div>
							        <div class="col-lg-9">
							        	<div class="row">
							        		<div class="col-lg-12">
							        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
							        		</div>
							        		<div class="col-lg-12">
							        			<h5 class="text-muted">1mo+24dy ago</h5>   
							        		</div>
							        	</div> 
							        </div>		        
							      </div>
					   			</div>
							</div>

			    		</div>
			    	</div>
			    </div>
			</div>
			    <div class="row" id="addtrack">
			    	<div class="col-lg-3">
			    		<button type="button" class="btn btn-default">SEE MORE...</button>
			    	</div>		
			    </div> 		
		</div>	

  <!--copyright-->
	    <div class="col-lg-6">
	    	<div class="row">
	    		<div class="col-lg-12" id="copyright">
	    			<div class="row">
			    		<div class="col-lg-12" id="judulcopyright">
			    			<h3>Copyright information</h3>
			    		</div>
				    	<div class="row">
				    		<div class="col-lg-12">

				    			<div class="container">
								    <div class="card bg-transparent" id="listactivity">
								      <div class="row">
								        <div class="col-lg-3" id="fotolatesactivity">
								            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
								        </div>
								        <div class="col-lg-9">
								        	<div class="row">
								        		<div class="col-lg-12">
								        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
								        		</div>
								        		<div class="col-lg-12">
								        			<h5 class="text-muted">1mo+24dy ago</h5>   
								        		</div>
								        	</div> 
								        </div>		        
								      </div>
						   			</div>
								</div>

								<div class="container">
								    <div class="card bg-transparent" id="listactivity">
								      <div class="row">
								        <div class="col-lg-3" id="fotolatesactivity">
								            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
								        </div>
								        <div class="col-lg-9">
								        	<div class="row">
								        		<div class="col-lg-12">
								        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
								        		</div>
								        		<div class="col-lg-12">
								        			<h5 class="text-muted">1mo+24dy ago</h5>   
								        		</div>
								        	</div> 
								        </div>		        
								      </div>
						   			</div>
								</div>

				    		</div>
				    	</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>






<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="<?= base_url('/assets/') ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= base_url('/assets/') ?>js/waveform-playlist.var.js"></script>
	<script type="text/javascript" src="<?= base_url('/assets/') ?>js/web-audio-editor.js"></script>
	<script type="text/javascript" src="<?= base_url('/assets/') ?>js/emitter.js"></script>
	<script src="https://unpkg.com/wavesurfer.js"></script>


<script>



	$('#volume').hide();
	$('#tombolAnu').hide();
    $('.track-drop').on(
        'drop',
        function(e){
            $('#volume').show();
            $('#tombolAnu').show();
        }
    );

    var wavesurfer = WaveSurfer.create({
        container: '#waveform1',
        waveColor: '#0F8EF9',
        progressColor: '#0F8EF9'
    });

    wavesurfer.load('<?= base_url('/assets/') ?>media/audio/BassDrums30.mp3');

    function play() {
        wavesurfer.play();
    }

    function pause() {
		wavesurfer.pause();
    }
</script>
</body>
</html>
