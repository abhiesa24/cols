<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Botstrap CSS -->
	<link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>css/login.css">

	<title>Collab Music</title>
</head>

<body>

<!-- navbar -->

<nav class="navbar justify-content-between">
	<a class="navbar-brand"></a>
	<form class="form-inline">
		<a class="navbar-brand" href="#">
			<img src="img/logo.png" width="30" height="30" alt="">
		</a>
		<a class="navbar-brand" href="#">
			<img src="img/logo.png" width="30" height="30" alt="">
		</a>
	</form>
</nav>

<!-- !navbar -->

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-sm-12 col-md-6 col-xs-12 mx-auto">
			<div class="row">

				<div class="col-lg-12">
					<img alt="mobil" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid">
				</div>

				<div class="col-lg-12 text-center" id="slogan">
					<h1>Upload.Colab.Create Arts.</h1>
				</div>

			</div>
		</div>

		<div class="col-lg-4 col-sm-12 col-md-6 col-xs-12 mx-auto">
			<div class="card card-signin my-5">

				<div id="card-bodyForgot" class="card-body">
					<h5 class="card-title text-center">Forgot Password</h5>
					<hr class="my-4">
					<!--- signUp --->


					<label for="inputEmail">New Password</label>
					<div class="form-group">
						<input type="password" id="password" class="form-control" placeholder="Password" required autofocus>

					</div>

					<label for="inputEmail">Confirm New Password</label>
					<div class="form-group">
						<input type="password" id="NewPassword" class="form-control" placeholder="Password" required autofocus>

					</div>

					<button class="btn btn-lg btn-primary btn-block text-uppercase" onclick="forgotPassword()" type="submit">Reset Password</button>
					<br>
<!--					<a class="float-right" href="#" onclick="signInForm()">Back to login</a>-->
					<hr class="my-4">


				</div>
			</div>
		</div>






	</div>
</div>


<!-- footer -->

<footer class="page-footer">

	<div class="footer-copyright py-3">
		<a href="#">Help</a>
		<a href="#">About</a>
		<a href="#">Privacy Police</a>
		<a href="#">Blog</a>
		<a href="#">Jobs</a>
		<a href="#">Terms</a>
		<a href="#">Cookies</a>
		<a href="#">Brands</a>
		<a href="#">Apps</a>
		<a href="#">Businesses</a>
		<a href="#">Developers</a>
		<a href="#">Settings</a>
		&copy;2018 Co-lab

	</div>

</footer>

<!-- footer -->


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="<?= base_url('/assets/') ?>js/bootstrap.min.js"></script>
<script src="<?= base_url('/assets/') ?>jquery/jquery-3.3.1.min.js"></script>

</body>

<script>


	function validateMandatory() {
		var password = document.getElementById("password").value;
		var newpassword = document.getElementById("NewPassword").value;

		if(password == "" || newpassword == ""){
			return 'failed';
		}else{
			if(password != newpassword){
				return 'failed';
			}else{
				return 'ok';
			}
		}
	}


	function forgotPassword() {

		var urlParams = new URLSearchParams(window.location.search);

		var token = urlParams.get('reset');

		var password = document.getElementById("password").value;
		var newpassword = document.getElementById("NewPassword").value;

		var result = validateMandatory();

		if(result == 'ok'){
			$.ajax({
				url: "<?php echo base_url('Register/setNewPassword'); ?>",
				type: "post",
				data: {
					password:password,
					token:token
				},
				cache: false,
				success: function (response) {
					// alert(response);
					if(response == "Ok"){
						alert('Password Berhasil di reset !')
					}else if(response == "Failed"){
						alert('Gagal reset password !');

					}
				}
			});
		}else{
			alert('Gagal Reset Password !');
		}

	}
</script>

</html>
