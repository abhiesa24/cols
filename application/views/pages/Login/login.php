<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Botstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>css/login.css">

    <title>Collab Music</title>
  </head>

  <body>

  <!-- navbar -->

	<nav class="navbar justify-content-between">
	  <a class="navbar-brand"></a>
	  <form class="form-inline">
	    <a class="navbar-brand" href="#">
		    <img src="img/logo.png" width="30" height="30" alt="">
	  	</a>
	  	<a class="navbar-brand" href="#">
	    	<img src="img/logo.png" width="30" height="30" alt="">
	  	</a>
	  </form>
	</nav>

  <!-- !navbar -->
    
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-sm-12 col-md-6 col-xs-12 mx-auto">
      	<div class="row">

      		<div class="col-lg-12">
         		<img alt="mobil" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid">
     		</div>

     		<div class="col-lg-12 text-center" id="slogan">
      			<h1>Upload.Colab.Create Arts.</h1>
      		</div>
      		
     	</div>
      </div>

      <div class="col-lg-4 col-sm-12 col-md-6 col-xs-12 mx-auto">
        <div class="card card-signin my-5">
          <div id="card-bodySignIn" class="card-body">

            <h5 class="card-title text-center">Sign In</h5>

            <hr class="my-4">

			
			<!-- signIn --->
			<label for="inputEmail">Email address</label>
              <div class="form-group">
                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                
              </div>

			  <label for="inputPassword">Password</label>
              <div class="form-group">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
               
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Remember</label>
              </div>

              <button onclick="LoginUser()" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
              <br>
              <a class="float-right" onclick="forgotPasswordForm()" href="#">Forgot password?</a>
              <hr class="my-4">

              <div class="row">
		         <div class="col-xs-12 col-lg-12 col-sm-6 col-md-6">
		           <a onclick="signUpForm()" class="btn btn-lg btn-primary btn-block">Create New Account</a>
		         </div>
		      </div>

          </div>
		  <div id="card-bodySignUp" class="card-body">
		  <h5 class="card-title text-center">Sign Up</h5>
		  <hr class="my-4">
		  <!--- signUp --->


			 <label for="inputEmail">Email address</label>
              <div class="form-group">
                <input type="email" id="emailSignUp" class="form-control" placeholder="Email address" required autofocus>
                
              </div>

			  <label for="inputPassword">Password</label>
              <div class="form-group">
                <input type="password" id="passwordSignUp" class="form-control" placeholder="Password" required>
              </div>
			  
			  <label for="inputPassword">Nama</label>
			  <div class="form-group">
                <input type="text" id="inputNamaSignUp" class="form-control" placeholder="Nama" required>
                
              </div>
			  
			   <label for="inputPassword">Alamat</label>
			  <div class="form-group">
                <input type="text" id="inputAlamatSignUp" class="form-control" placeholder="Alamat" required>
               
              </div>

              <button class="btn btn-lg btn-primary btn-block text-uppercase" onclick="registerUser()">Create New Account</button>
              <br>
              <a class="float-right" href="#" id="clickSignIn" onclick="signInForm()">Already have account</a>
              <hr class="my-4">

             


		  </div>

			<div id="card-bodyForgot" class="card-body">
				<h5 class="card-title text-center">Forgot Password</h5>
				<hr class="my-4">
				<!--- signUp --->


					<label for="inputEmail">Email address</label>
					<div class="form-group">
						<input type="email" id="emailForgot" class="form-control" placeholder="Email address" required autofocus>

					</div>

					<button class="btn btn-lg btn-primary btn-block text-uppercase" onclick="forgotPassword()" type="submit">Send Password Reset</button>
					<br>
					<a class="float-right" href="#" onclick="signInForm()">Back to login</a>
					<hr class="my-4">


			</div>
        </div>
      </div>






    </div>
  </div>


  <!-- footer -->

  <footer class="page-footer">

	  <div class="footer-copyright py-3">
	    <a href="#">Help</a>
	    <a href="#">About</a>
	    <a href="#">Privacy Police</a>
		<a href="#">Blog</a>
		<a href="#">Jobs</a>
		<a href="#">Terms</a>
		<a href="#">Cookies</a>
		<a href="#">Brands</a>
		<a href="#">Apps</a>
		<a href="#">Businesses</a>
		<a href="#">Developers</a>
		<a href="#">Settings</a>
		&copy;2018 Co-lab 

	  </div>

  </footer>

  <!-- footer -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="<?= base_url('/assets/') ?>js/bootstrap.min.js"></script>
    <script src="<?= base_url('/assets/') ?>jquery/jquery-3.3.1.min.js"></script>

  </body>
  
  <script>
	  document.getElementById("card-bodySignUp").hidden = true;
	  document.getElementById("card-bodyForgot").hidden = true;

	  function signUpForm(){
		  document.getElementById("card-bodySignIn").hidden = true;
		  document.getElementById("card-bodyForgot").hidden = true;
		  document.getElementById("card-bodySignUp").hidden = false;
	  }

	  function signInForm(){
		  document.getElementById("card-bodySignIn").hidden = false;
		  document.getElementById("card-bodySignUp").hidden = true;
		  document.getElementById("card-bodyForgot").hidden = true;
	  }

	  function forgotPasswordForm(){
		  document.getElementById("card-bodyForgot").hidden = false;
		  document.getElementById("card-bodySignIn").hidden = true;
		  document.getElementById("card-bodySignUp").hidden = true;
	  }

	  function validateMandatory() {
		  var email = document.getElementById("emailSignUp").value;
		  var password = document.getElementById("passwordSignUp").value;
		  var nama = document.getElementById("inputNamaSignUp").value;
		  var alamat = document.getElementById("inputAlamatSignUp").value;

		  if(email == "" || password == "" || nama == ""
			  || alamat == ""){
			  return 'failed';
		  }else{
			  return 'ok';
		  }
	  }

	  function cleanFormReg() {
		  document.getElementById("emailSignUp").value = "";
		  document.getElementById("inputNamaSignUp").value = "";
		  document.getElementById("inputAlamatSignUp").value = "";
		  document.getElementById("passwordSignUp").value = "";
	  }
	  function registerUser() {
		  var email = document.getElementById("emailSignUp").value;
		  var password = document.getElementById("passwordSignUp").value;
		  var nama = document.getElementById("inputNamaSignUp").value;
		  var alamat = document.getElementById("inputAlamatSignUp").value;

		  var result = validateMandatory();

		  if(result == 'ok'){
			  $.ajax({
				  url: "<?php echo base_url('Register/RegUser'); ?>",
				  type: "post",
				  data: {
					  email:email,
					  password:password,
					  nama:nama,
					  alamat:alamat,
				  },
				  cache: false,
				  success: function (response) {
					  // alert(response);
					  if(response == "Ok"){
						  alert('Resgiter sukses, silahkan konfirmasi email');
						  //cleanFormReg();
						  $('#clickSignIn').click();
					  }else if(response == "Failed"){
						  alert('Gagal Register !');
					  }
				  }
			  });
		  }
		  else{
			  alert('Failed Register !');
		  }

	  }

	  function LoginUser() {

		  var email = document.getElementById("inputEmail").value;
		  var password = document.getElementById("inputPassword").value;

		  $.ajax({
			  url: "<?php echo base_url('Login/loginUser'); ?>",
			  type: "post",
			  data: {
				  email:email,
				  password:password,
			  },
			  cache: false,
			  success: function (response) {
				  // alert(response);
				  if(response == "Ok"){
					  console.log('Ok');
					  window.location.href = '<?= base_url('Home') ?>';
				  }else if(response == "Failed"){
					  alert('Gagal Login !');
				  }
			  }
		  });

	  }

	  function forgotPassword() {
		  var email = document.getElementById("emailForgot").value;

		  $.ajax({
			  url: "<?php echo base_url('Register/ForgotPassword'); ?>",
			  type: "post",
			  data: {
				  email:email
			  },
			  cache: false,
			  success: function (response) {
				  // alert(response);
				  if(response == "Ok"){
					  alert('link reset password berhasil dikirim ke email !')
				  }else if(response == "Failed"){
					  alert('Email tidak terdaftar !');

				  }
			  }
		  });
	  }

  </script>
  
</html>
