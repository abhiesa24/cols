<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Botstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>open-iconic-master/font/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>css/home.css"> 

    <title>Home</title>
  </head>

  <body>

  <!-- navbar -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top">
		<div class="container-fluid">
  		<a class="navbar-brand" href="<?= base_url('/assets/') ?>home.html">
    		<img src="<?= base_url('/assets/') ?>img/logo.png" width="30" height="30" alt="">
  		</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   		<span class="navbar-toggler-icon"></span>
  		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mx-auto">
				<li class="nav-item">
		        	<form class="navbar-form" role="search">
        				<div class="input-group">
            				<input type="text" class="form-control" placeholder="Search" name="q">
            				<div class="input-group-btn">
                			<button class="btn btn-default" type="submit"><i class="oi oi-magnifying-glass"></i>
                			</button>
            					</div>
        				</div>
        			</form>
		  		</li>
		  	</ul>

		  	<ul class="navbar-nav">
			    <li class="nav-item dropdown">
				    <a class="nav-link dropdown-toggle" id="notification" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
				        <i class="oi"><img src="<?= base_url('/assets/') ?>icon/si-glyph-bell.svg" class="img-fluid">
				          <span class="badge badge-danger">25</span>
				        </i>
				    </a>
				    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="notification">
				          <a class="dropdown-item" href="#">Action</a>
				          <a class="dropdown-item" href="#">Another action</a>
				          <a class="dropdown-item" href="#">Something else here</a>
			        </div>
				</li>

			    <li class="nav-item dropdown">
				    <a class="nav-link dropdown-toggle" id="message" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
				    	<i class="oi"><img src="<?= base_url('/assets/') ?>icon/si-glyph-mail.svg" class="img-fluid">
				          <span class="badge badge-danger">25</span>
				        </i>
				    </a>
				        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="message">
				          <a class="dropdown-item" href="#">Action</a>
				          <a class="dropdown-item" href="#">Another action</a>
				          <a class="dropdown-item" href="#">Something else here</a>
			        	</div>
				</li>

			  	<li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle oi oi-menu" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			        </a>
			        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="<?= base_url('Profile') ?>">Profile</a>
						<a class="dropdown-item" href="<?= base_url('Login/loogOut') ?>">Logout</a>
			        </div>
			    </li>

			</ul>
		</div>
		</div>
	</nav>

  <!-- !navbar -->
  
    <div class="container-fluid" id="containermenu">
    <div class="row profile">

		<div class="col-lg-9" id="listlagu">
			<div class="row">
				<div class="col-lg-12" id="putarlagu">
					<div class="scrollable" id="style-1">

						<!--card list music-->
						<div class="container py-1">
					    <div class="card" id="card">
					      <div class="row ">
					        <div class="col-md-2" id="logolistmusik">
					            <img src="<?= base_url('/assets/') ?>icon/si-glyph-document-music.svg"/>
					        </div>
					        <div class="col-md-8 px-3">
					            <div class="card-block px-3">
					            	<a href="">
					              <h4 id="title1" class="card-title">Wherever You Are - Drum</h4></a>
					              <a href="<?= base_url('Profile') ?>"><p class="card-text"><small>Alif</small></p></a>
									<div id="waveform1"></div>
					              <div class="card-footer bg-transparent " id="commentlikeicon">
					              	<div class="row">
					              		<div class="col-lg-5">
					              			<img src="<?= base_url('/assets/') ?>icon/si-glyph-bubble-message.svg"/ data-toggle="modal" data-target="#comments_view">1
					              		</div>
					              		<div class="col-lg-5">
					              			<img src="<?= base_url('/assets/') ?>icon/si-glyph-like.svg"/>5
					              		</div>
					              	</div>
					              </div>            
					            </div>
					        </div>
					        <div class="col-md-2" id="logolistmusikplay">
					            <div class="tombolplay" data-toggle="buttons">
						            <label class="btn btn-lg active">
						                <input type="radio" name="options" id="option1" autocomplete="off" checked>
						                <i><img onclick="pause()" src="<?= base_url('/assets/') ?>icon/si-glyph-pause.svg"/></i>
						            </label>
						            <label class="btn btn-lg">
						                <input type="radio" name="options" id="option2" autocomplete="off">
						                <i><img onclick="play()" src="<?= base_url('/assets/') ?>icon/si-glyph-triangle-right.svg"/></i>
						            </label>          
						        </div>
							</div>				        
					      </div>
			   			</div>
						</div>
						<div class="modal fade product_view" id="comments_view">
						    <div class="modal-dialog">
						        <div class="modal-content">
						            <div class="modal-header">
						                <a href="#" data-dismiss="modal" class="class"></a>
						                <h3 class="modal-title">Wherever You Are - Drum</h3>
						            </div>
						            <div class="modal-body">
						                <div class="row">
						                    <div class="col-md-12 product_img">
						                      <div class="row">
									    		<div id="commentArea" class="col-lg-12">

									    			<div class="container">
													    <div class="card bg-transparent" id="listactivity">
													      <div class="row">
													        <div class="col-lg-3" id="fotolatesactivity">
													            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
													        </div>
													        <div class="col-lg-9">
													        	<div class="row">
													        		<div class="col-lg-12">
													        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
													        		</div>
													        		<div class="col-lg-12">
													        			<h5 class="text-muted">1mo+24dy ago</h5>   
													        		</div>
													        	</div> 
													        </div>		        
													      </div>
											   			</div>
													</div>


												</div>
											</div>

						                    </div>
						                </div>
						            </div>
						            <div class="modal-footer">
					                    <div class="input-group">
					                        <input type="text" id="commentText" class="form-control input-sm" placeholder="Type your message here..." />
					                        <span class="input-group-btn">
					                            <button onclick="sendComment()" class="btn btn-warning btn-sm" id="btn-chat">
					                                Send</button>
					                        </span>
					                    </div>
						            </div>
						        </div>
						    </div>
						</div>

						<!--card list music-->

						<!--card list music-->
						<div class="container py-1">
					    <div class="card" id="card">
					      <div class="row ">
					        <div class="col-md-2" id="logolistmusik">
					            <img src="<?= base_url('/assets/') ?>icon/si-glyph-document-music.svg"/>
					        </div>
					        <div class="col-md-8 px-3">
					            <div class="card-block px-3">
					            	<a href="">
					              <h4 id="title2" class="card-title">Anything - Vocals</h4></a>
					              <p class="card-text"><small>Kevin</small></p>
									<div id="waveform2"></div>
					              <div class="card-footer bg-transparent " id="commentlikeicon">
					              	<div class="row">
					              		<div class="col-lg-5">
					              			<img src="<?= base_url('/assets/') ?>icon/si-glyph-bubble-message.svg"/ data-toggle="modal" data-target="#comments_view">1
					              		</div>
					              		<div class="col-lg-5">
					              			<img src="<?= base_url('/assets/') ?>icon/si-glyph-like.svg"/>5
					              		</div>
					              	</div>
					              </div>            
					            </div>
					        </div>
					        <div class="col-md-2" id="logolistmusikplay">
					            <div class="tombolplay" data-toggle="buttons">
						            <label class="btn btn-lg active">
						                <input type="radio" name="options" id="option1" autocomplete="off" checked>
						                <i><img onclick="pause2()" src="<?= base_url('/assets/') ?>icon/si-glyph-pause.svg"/></i>
						            </label>
						            <label class="btn btn-lg">
						                <input type="radio" name="options" id="option2" autocomplete="off">
						                <i><img onclick="play2()" src="<?= base_url('/assets/') ?>icon/si-glyph-triangle-right.svg"/></i>
						            </label>          
						        </div>
							</div>				        
					      </div>
			   			</div>
						</div>
						<div class="modal fade product_view" id="comments_view">
						    <div class="modal-dialog">
						        <div class="modal-content">
						            <div class="modal-header">
						                <a href="#" data-dismiss="modal" class="class"></a>
						                <h3 class="modal-title">Melodi 1</h3>
						            </div>
						            <div class="modal-body">
						                <div class="row">
						                    <div class="col-md-12 product_img">
						                      <div class="row">
									    		<div class="col-lg-12">

									    			<div class="container">
													    <div class="card bg-transparent" id="listactivity">
													      <div class="row">
													        <div class="col-lg-3" id="fotolatesactivity">
													            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">
													        </div>
													        <div class="col-lg-9">
													        	<div class="row">
													        		<div class="col-lg-12">
													        			<h5 class="">drumdawg accepted the mix DirtyDirtyRice uploaded to the collaboration Dance Ballarina,Dance</h5>   
													        		</div>
													        		<div class="col-lg-12">
													        			<h5 class="text-muted">1mo+24dy ago</h5>   
													        		</div>
													        	</div> 
													        </div>		        
													      </div>
											   			</div>
													</div> 
												</div>
											</div>

						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</div>

						<!--card list music-->
					</div>
				</div>

				<div class="col-lg-12" id="putarlagu2">
					<div class="container py-1">
					    <div class="card" id="card">
					      <div class="row">
					        <div class="col-md-2" id="logomusik">
					            <img src="<?= base_url('/assets/') ?>icon/si-glyph-document-music.svg" class="img-fluid mx-auto d-block">
					        </div>
					        <div class="col-md-4">
					            <div class="card-block">
					              <h4 id="titlePlay" class="card-title">Untitled</h4>
					              <p class="card-text"><small>Kevin</small></p>              
					            </div>
					        </div>
					        <div class="col-md-2" id="logomusik">
					            <img src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid">
					        </div>
					        <div class="col-md-4" id="buttonplaypause">
					            <div class="tombolplay2" data-toggle="buttons">
					            	<img src="<?= base_url('/assets/') ?>icon/si-glyph-jump-backward.svg" class="img-fluid">
						            <label class="btn btn-lg active">
						                <input type="radio" name="options" id="option1" autocomplete="off" checked>
						                <i><img id="pauseBtn" src="<?= base_url('/assets/') ?>icon/si-glyph-pause.svg" class="img-fluid" /></i>
						            </label>
						            <label class="btn btn-lg">
						                <input type="radio" name="options" id="option2" autocomplete="off">
						                <i><img id="PlayBtn" src="<?= base_url('/assets/') ?>icon/si-glyph-triangle-right.svg" class="img-fluid" /></i>
						            </label>
						            <img src="<?= base_url('/assets/') ?>icon/si-glyph-jump-forward.svg" class="img-fluid">          
						        </div>
					            
					        </div>
					      </div>
						</div>
					</div>

				</div>
			</div>

		</div>

		<div class="col-lg-3" id="sidekanan">
			<div class="row">
				<div class="col-12" id="menubutton">
					<div class="row">
						<div class="col-12">
							<a href="#" class="btn-1 mx-auto d-block">Home</a>								
						</div>
						<div class="col-12">
							<a href="" class="btn-1 mx-auto d-block">Explore</a>
						</div>
						<div class="col-12">
							<a href="" class="btn-1 mx-auto d-block">Hover me</a>
						</div>
					</div>
				</div>
			</div>

			<!--timeline-->
			
				<div class="col-12" id="timeline">		
					<div class="scrollable" id="style-1"><div class="row">
					
					<div class="container py-1">
					    <div class="card bg-transparent" id="timelineCard">
					      <div class="row">
					        <div class="col-lg-3" id="fotouser">
					            <img src="<?= base_url('/assets/') ?>img/logo.png" class=" mx-auto d-block img-fluid">
					        </div>
					        <div class="col-lg-9">
					            <div class="card-block">
					              <h5 class="card-title">KEVIN EZA RIZKY upload new TRACK</h5>         
					            </div>
					        </div>			        
					      </div>
			   			</div>
					</div>

					<div class="container py-1">
					    <div class="card bg-transparent" id="timelineCard">
					      <div class="row">
					        <div class="col-lg-3" id="fotouser">
					            <img src="<?= base_url('/assets/') ?>img/logo.png" class=" mx-auto d-block img-fluid">
					        </div>
					        <div class="col-lg-9">
					            <div class="card-block">
					              <h5 class="card-title">KEVIN EZA RIZKY upload new TRACK</h5>         
					            </div>
					        </div>			        
					      </div>
			   			</div>
					</div>

					<div class="container py-1">
					    <div class="card bg-transparent" id="timelineCard">
					      <div class="row">
					        <div class="col-lg-3" id="fotouser">
					            <img src="<?= base_url('/assets/') ?>img/logo.png" class=" mx-auto d-block img-fluid">
					        </div>
					        <div class="col-lg-9">
					            <div class="card-block">
					              <h5 class="card-title">KEVIN EZA RIZKY upload new TRACK</h5>         
					            </div>
					        </div>			        
					      </div>
			   			</div>
					</div>

					<div class="container py-1">
					    <div class="card bg-transparent" id="timelineCard">
					      <div class="row">
					        <div class="col-lg-3" id="fotouser">
					            <img src="<?= base_url('/assets/') ?>img/logo.png" class=" mx-auto d-block img-fluid">
					        </div>
					        <div class="col-lg-9">
					            <div class="card-block">
					              <h5 class="card-title">KEVIN EZA RIZKY upload new TRACK</h5>         
					            </div>
					        </div>			        
					      </div>
			   			</div>
					</div>

					<div class="container py-1">
					    <div class="card bg-transparent" id="timelineCard">
					      <div class="row">
					        <div class="col-lg-3" id="fotouser">
					            <img src="<?= base_url('/assets/') ?>img/logo.png" class=" mx-auto d-block img-fluid">
					        </div>
					        <div class="col-lg-9">
					            <div class="card-block">
					              <h5 class="card-title">KEVIN EZA RIZKY upload new TRACK</h5>         
					            </div>
					        </div>			        
					      </div>
			   			</div>
					</div>



				</div>
			</div>
		</div>

			<!--timeline-->

			

		</div>

	</div>
</div>

    
  


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="<?= base_url('/assets/') ?>js/bootstrap.min.js"></script>
  	<script src="https://unpkg.com/wavesurfer.js"></script>
    


  </body>

<script>
    var wavesurfer = WaveSurfer.create({
        container: '#waveform1',
        waveColor: '#0F8EF9',
        progressColor: '#0F8EF9'
    });

    var wavesurfer1 = WaveSurfer.create({
        container: '#waveform2',
        waveColor: '#0F8EF9',
        progressColor: '#0F8EF9'
    });

    wavesurfer.load('<?= base_url('/assets/') ?>media/audio/BassDrums30.mp3');
    wavesurfer1.load('<?= base_url('/assets/') ?>media/audio/Vocals30.mp3');

    function play() {
        wavesurfer.play();
        var title = $('#title1').text();
        $('#titlePlay').text(title);
        $('#PlayBtn').click();

    }

    function pause() {
        wavesurfer.pause();
       // var title = $('#title1').text();
       // $('#titlePlay').text(title);
        $('#pauseBtn').click();
    }

    function play2() {
        wavesurfer1.play();
        var title = $('#title2').text();
        $('#titlePlay').text(title);
        $('#PlayBtn').click();

    }

    function pause2() {
        wavesurfer1.pause();
        $('#pauseBtn').click();
    }
    
    function sendComment() {

        var text = document.getElementById("commentText").value;
		$('#commentArea').append('<div class="container">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t    <div class="card bg-transparent" id="listactivity">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t      <div class="row">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        <div class="col-lg-3" id="fotolatesactivity">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t            <img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        </div>\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        <div class="col-lg-9">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t<div class="row">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t\t<div class="col-lg-12">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t\t\t<h5 class="">' + text + '</h5>   \n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t\t</div>\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t\t<div class="col-lg-12">\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t\t\t<h5 class="text-muted">now</h5>   \n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t\t</div>\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        \t</div> \n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t        </div>\t\t        \n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t      </div>\n' +
            '\t\t\t\t\t\t\t\t\t\t\t   \t\t\t</div>\n' +
            '\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n')
    }
</script>
</html>
