<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 02/10/2018
 * Time: 15:03
 */
?>
<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Botstrap CSS -->
	<link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url('/assets/') ?>css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>open-iconic-master/font/css/open-iconic-bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/') ?>css/home.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<title><?= $title ?></title>
</head>

<body>

<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
	<div class="container-fluid">
		<a class="navbar-brand" href="home.html">
			<img src="<?= base_url('/assets/') ?>img/logo.png" width="30" height="30" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mx-auto">
				<li class="nav-item">
					<form class="navbar-form" role="search">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search" name="q">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="oi oi-magnifying-glass"></i>
								</button>
							</div>
						</div>
					</form>
				</li>
			</ul>

			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">
						<i class="oi"><img src="<?= base_url('/assets/') ?>icon/si-glyph-bell.svg" class="img-fluid">
							<span class="badge badge-danger">25</span>
						</i>
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="#">
						<i class="oi"><img src="<?= base_url('/assets/') ?>icon/si-glyph-mail.svg" class="img-fluid">
							<span class="badge badge-danger">25</span>
						</i>
					</a>
				</li>

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle oi oi-menu" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>

<!-- !navbar -->
