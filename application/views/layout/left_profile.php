<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 02/10/2018
 * Time: 15:04
 */
?>

<div class="container-fluid" id="containermenu">
	<div class="row profile">
		<div class="col-lg-3" id="profile">
			<div class="row">
				<div class="col-lg-12" id="foto">
					<div class="view overlay hm-zoom">
						<img alt="Photo" src="<?= base_url('/assets/') ?>img/logo.png" class="img-fluid mx-auto d-block" width="200">
					</div>
				</div>
				<div class="col-lg-12 text-center" id="nama">
					<h2>KEVIN EZA RIZKY</h2>

				</div>
				<div class="col-lg-12 text-center" id="status">
					<div class="row">
						<div class="col" id="listener">
							<h3>Listener</h3>
							<h5><label id="ListenerNumber"></label></h5>
						</div>
						<div class="col" id="upload">
							<h3>Upload</h3>
							<h5>50</h5>
						</div>
						<div class="w-100"></div>
						<div class="col" id="likes">
							<h3>Likes</h3>
							<h5>200000</h5>
						</div>
						<div class="col" id="colab">
							<h3>Colab</h3>
							<h5>2</h5>
						</div>
					</div>
				</div>

				<div class="col-lg-12" id="uploadfile">
					<a href="">
						<img class="mx-auto d-block zoom" src="<?= base_url('/assets/') ?>icon/si-glyph-file-upload.svg"/><br>
					</a><h5 class="text-center">UPLOAD OR DRAG FILE</h5>
				</div>
			</div>
		</div>


		<div class="col-lg-6" id="listlagu">
			<div class="row">
				<div class="col-lg-12" id="putarlagu">
					<div class="scrollable" id="style-1">

						<!--card list music-->

