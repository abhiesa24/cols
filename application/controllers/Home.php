<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 01/10/2018
 * Time: 22:43
 */

class Home extends CI_Controller
{

	function __construct(){
		parent::__construct();
		//$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->helper('string');
	}

	public function index(){
		$token = $this->session->userdata('token');

		if(!empty($token)){
			//$data = array('isi' => 'Pages/Home/index', 'title' => 'Profile');
			$this->load->view('Pages/Home/index');
		}else{
			echo "Please Login !";
		}
	}

	public function getListener(){

		$token = $_POST['token'];

		$result = $this->Home_model->getListener($token);

		echo json_encode($result);

	}
}
