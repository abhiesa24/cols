<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 29/09/2018
 * Time: 23:24
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{

	function __construct(){
		parent::__construct();
		//$this->load->library('session');
		$this->load->model('Register_model');
		$this->load->helper('string');
	}

	public function index(){
		$this->load->view('pages/Login/login');
	}

	public function RegUser(){
		$email = $_POST['email'];
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$password = $_POST['password'];

		$data = array(
			'email' => $email,
			'nama' => $nama,
			'alamat' => $alamat,
			'password' => hash('sha256', $password),
			'flag' => '0'
		);

		$resultRegister = $this->Register_model->insertData($data);

		if(!empty($resultRegister)){
			$data2 = array(
				'token' => md5($email),
				'idUser' => $resultRegister,
				'exp' => '1'
			);
			$resultSession = $this->Register_model->insertSession($data2);
			if($resultSession > 0){
				echo "Ok";
//				$status = 'register';
//				$token = md5($email);
//				sendMail($email,$token,$status);
			}else{
				echo "Failed";
			}
		}else{
			echo "Failed";
		}
	}

	public function Confirm(){
		$token = $_GET['success'];

		$result = $this->Register_model->FindToken($token);

		if(!empty($result)){

			$flag = array(
				'flag' => '1'
			);
			$resultConfirm = $this->Register_model->Confirm($result[0]['idUser'],$flag);
			if($resultConfirm > 0){
				echo "Ok";
				$exp = array(
					'exp' => '0'
				);
				$this->Register_model->ExpToken($result[0]['idUser'],$exp);
			}else{
				echo "Failed";
			}
		}else{
			echo "Failed";
		}
	}

	public function ForgotPassword(){
		$email = $_POST['email'];

		$resultEmail = $this->Register_model->getEmail($email);

		//echo json_encode($resultEmail);
		if(!empty($resultEmail)){
			$token = array(
				'token' => md5($email.''.date("Y-m-d H:i:s")),
				'exp' => '1'
			);
			$resultSetToken = $this->Register_model->setToken($resultEmail[0]['idUser'], $token);
			if($resultSetToken > 0){
				echo "Ok";
//				$status = 'register';
//				$token =  md5($email.''.date("Y-m-d H:i:s"));
//				sendMail($email,$token,$status);
			}else{
				echo "Failed";
			}
		}
	}

	public function confirmForgot(){
		$token = $_GET['reset'];
		$result = $this->Register_model->FindToken($token);

		if(!empty($result)){
			$this->load->view('pages/Login/ForgotPassword');
		}else{
			echo "Failed";
		}
	}

	public function setNewPassword(){
		$password = $_POST['password'];
		$token = $_POST['token'];

		$resultData = $this->Register_model->getDataUser($token);

		if(!empty($resultData)){
			$data = array(
				'password' => hash('sha256', $password),
			);
			$result = $this->Register_model->setNewPassword($resultData[0]['idUser'],$data);
			if($result > 0){
				$dataToken = array(
					'exp' => '0'
				);
				$setExp = $this->Register_model->ExpToken($resultData[0]['idUser'],$dataToken);
				if($setExp > 0 ){
					echo "Ok";
				}else{
					echo "Failed";
				}
			}else{
				echo "Failed";
			}
		}else{
			echo "Failed";
		}


	}

	public function sendMail($email, $token, $status){
			$this->load->library('email');

			if($status == 'register'){
				$msg = '<p>Dear sahabat musisi,</p>

			<p>Selamat!!! Tinggal satu langkah lagi untuk menciptakan 
			<em><strong>Magnum Opus</strong></em> anda. Silahkan klik link dibawah ini untuk aktivasi akun anda</p>
			
			<p>' . base_url('Register/Confirm/?success=') .' '. $token .'</p>
			<hr>
			
			<p><span style="color: rgb(84, 172, 210);">Kind Regars,</span></p>
			
			<p><span style="color: rgb(84, 172, 210);">Team Collabs.</span></p>
			
			<p><span style="color: rgb(84, 172, 210);">abhiesa24@gmail.com</span></p>
			';
			}else{
				$msg = '<p>Dear sahabat musisi,</p>

			<p>Untuk melakukan reset pada akun anda, silahkan klik link dibawah ini</p>
			
			<p>' . base_url('Register/Confirm/?success=') .' '. $token .'</p>
			<hr>
			
			<p><span style="color: rgb(84, 172, 210);">Kind Regars,</span></p>
			
			<p><span style="color: rgb(84, 172, 210);">Team Collabs.</span></p>
			
			<p><span style="color: rgb(84, 172, 210);">abhiesa24@gmail.com</span></p>
			';
			}

			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->to($email);
			$this->email->cc('abhiesa24@gmail.com');
			$this->email->from('abhiesa24@gmail.com','Colab Team');
			$this->email->subject('Aktivasi Akun Colab');
			$this->email->message($msg);

			if(!$this->email->send()){
				return 'gagal';
			}else{
				return 'sukses';
			}

	}


}
