<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 01/10/2018
 * Time: 19:37
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	function __construct(){
		parent::__construct();
		//$this->load->library('session');
		$this->load->model('Login_model');
		$this->load->helper('string');
	}

	public function loginUser(){
		$email = $_POST['email'];
		$passwod = hash('sha256', $_POST['password']);

		$resultLogin = $this->Login_model->loginUser($email,$passwod);

		if(!empty($resultLogin)){

			$data = array(
				'idUser' => $resultLogin[0]['idUser'],
				'token' => md5($email.''.date("Y-m-d H:i:s"))
			);
			$saveToken = $this->Login_model->saveToken($resultLogin[0]['idUser'],$data);


			if($saveToken > 0){

				$response = $this->Login_model->getData($resultLogin[0]['idUser']);

				$loginData2 = array(
					"token" => $response[0]['token'],
					"email" => $response[0]['email'],
					"nama" => $response[0]['nama']
				);
				$this->session->set_userdata($loginData2);
				//echo json_encode($response);
				echo "Ok";
			}else{
				echo "Failed";
			}
		}else{
			echo "Failed";
		}
	}

	public function loogOut(){
		session_destroy();

		header("Location:". base_url());
	}
}
