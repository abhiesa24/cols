<?php
/**
 * Created by IntelliJ IDEA.
 * User: UnixMan
 * Date: 04/10/2018
 * Time: 20:07
 */
class Collab extends CI_Controller{

	public function index(){
		$token = $this->session->userdata('token');

		if(!empty($token)){
			//$data = array('isi' => 'Pages/Home/index', 'title' => 'Profile');
			$this->load->view('Pages/Collab/index');
		}else{
			echo "Please Login !";
		}
	}
}
